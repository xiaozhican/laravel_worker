<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/7/10
 * Time: 15:05
 */
namespace app\Admin\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Assistant;
use Encore\Admin\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class AssistantController extends Controller{
    public function index(Content $content){
        //return view('assistant.index');
        $content->header('活动列表');
        $grid = new Grid(new Assistant());
        $grid->quickSearch('activity_name');
        $grid->column('id','ID')->display(function ($title){
            return "<span style='color: red'>$title</span>";
        });
        $grid->column('activity_name','活动名称');
        $grid->paginate(10);
        return $content;
    }
    public function form(){
        return Admin::form();
    }
    public function addWatermark(){
        $img = \Image::make(public_path('images/main.png'));
        $img->insert(public_path('watermark.png'),'bottom-right',10, 10);
        $img->save();
    }
}