<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/7/23
 * Time: 15:49
 */
namespace App\Http\Controllers;

use App\Models\Assistant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MemberController extends Controller{
    //导出
    public function export(Request $request){
        $type = $request->input('types', 1);

        $users = Assistant::all();

        if (!$users->count()) {
            return response()->json([
                'code' => '201',
                'status' => 'error',
                'msg' => '暂无数据',
            ]);
        }

        if ($type != 1) {
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'msg' => '导出成功'
            ]);
        }

        /**
         * 数据处理
         */

//        $cellData['title'] = ['表头'];
//        $cellData['columns'] = ['A1', 'B1', 'C1']; // 表头列名
//        $cellData['body'] = $users;
//
//        \Maatwebsite\Excel\Facades\Excel::create(iconv('utf-8', 'gbk', '文件名'), function ($excel) use ($cellData) {
//            $excel->sheet('score', function ($sheet) use ($cellData) {
//                // 表头插入
//                $sheet->appendRow(1, $cellData['id']);
//                $sheet->appendRow(2, $cellData['activity_name']);
//
//                // 填充数据
//                $sheet->rows($cellData['body']);
//
//                // 设置列宽
//                $sheet->setWidth([
//                    'A1'=>'50px',
//                    'B1'=>'100px',
//                    'C1'=>'150px'
//                ]);
//
//                // 冻结表头
//                $sheet->setFreeze('A3');
//
//                // 横向 合并单元格 A1 到 C1
//                $sheet->mergeCells("A1:C1");
//
//                // 纵向合并
//                $sheet->setMergeColumn(array(
//                    'columns' => array('B'),  // 列数
//                    // 行数，一个二维数组
//                    'rows' => array(
//                        array(3, 5),
//                        array(6, 14),
//                        array(15, 23),
//
//                    )
//                ));
//
//                // 字体居中
//                $sheet->cells("A1:C23", function ($cells) {
//                    $cells->setAlignment('center');
//                    $cells->setValignment('center');
//                });
//            });
//        })->export('xls', ['Set-Cookie' => 'fileDownload=true; path=/']);
        return view('member.export');
    }
    public function exportexcel(Request $request)
    {
        if($request->isMethod('post')){
            dd(1111);
            //Assistant::all();
            $cellData = [
                ['学号','姓名','成绩'],
                ['10001','AAAAA','99'],
                ['10002','BBBBB','92'],
                ['10003','CCCCC','95'],
                ['10004','DDDDD','89'],
                ['10005','EEEEE','96'],
            ];
            \Maatwebsite\Excel\Facades\Excel::create('学生成绩',function ($excel) use ($cellData){
                $excel->sheet('score', function ($sheet) use ($cellData){
                    $sheet->rows($cellData);
                });
            })->export('xls');
        }
        return view('member.exportexcel');
    }
    public function download(Request $request){
        $activity_name = $request->get('activity_name');
        $export = $request->get('export');
        $query = Assistant::orderBy('create_time','desc');
        if($activity_name){
            $query->where('activity_name',"like","%$activity_name%");
        }
        if($export == 1){
            dd($query->toSql());
            $cellData = $query->selectRaw('activity_name,activity_content,img_path,activity_time,activity_address,activity_num,state,user_id,create_time')->get()->toArray();//表中取出显示的字段

            $title = array('活动名称','活动内容','图片路径','活动时间','活动地点','活动人数','状态','发布者id','创建时间');
            for($i=0;$i<count($cellData);$i++){
                $cellData[$i] = array_values($cellData[$i]);
            }
            //dd($cellData);
            array_unshift($cellData,$title);
            $filename = date('YmdHis').rand(100000,999999);
            \Maatwebsite\Excel\Facades\Excel::create($filename,function($excel) use ($cellData){
                $excel->sheet('score', function($sheet) use ($cellData){
                    $sheet->rows($cellData);
                });
            })->export('xls');
        }
        $list = $query->paginate(10)->appends($request->all());
        if($request->isMethod('post')){
            ini_set('memory_limit','500M');
            set_time_limit(0);//设置超时限制为0分钟
            $cellData = Assistant::select('activity_name','activity_content','img_path','activity_time','activity_address','activity_num','state','user_id','create_time')->get()->toArray();//表中取出显示的字段
            $title = array('活动名称','活动内容','图片路径','活动时间','活动地点','活动人数','状态','发布者id','创建时间');
            for($i=0;$i<count($cellData);$i++){
                $cellData[$i] = array_values($cellData[$i]);
            }
            //dd($cellData);
            array_unshift($cellData,$title);
            $filename = date('YmdHis').rand(100000,999999);
            \Maatwebsite\Excel\Facades\Excel::create($filename,function($excel) use ($cellData){
                $excel->sheet('score', function($sheet) use ($cellData){
                    $sheet->rows($cellData);
                });
            })->export('xls');
        }
        return view('member.download',['list'=>$list]);
    }
    public function download1(){
        ini_set('memory_limit','500M');
        set_time_limit(0);//设置超时限制为0分钟
        $cellData = Assistant::select('activity_name','activity_content','img_path','activity_time','activity_address','activity_num','state','user_id','create_time')->get()->toArray();//表中取出显示的字段
        dd($cellData);
        $title = array('活动名称','活动内容','图片路径','活动时间','活动地点','活动人数','状态','发布者id','创建时间');
        for($i=0;$i<count($cellData);$i++){
            $cellData[$i] = array_values($cellData[$i]);
        }
        //dd($cellData);
        array_unshift($cellData,$title);
        $filename = date('YmdHis').rand(100000,999999);
        \Maatwebsite\Excel\Facades\Excel::create($filename,function($excel) use ($cellData){
            $excel->sheet('score', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->export('xls');
        return view('member.download1');
    }
    /**
     *
     * Excel导入，xlsx格式
     */
    public function import(Request $request){
        if($request->isMethod('post')) {
            $file = $request->file('file');
            if($file->isValid()){
                $realPath = $file->getRealPath();
                \Maatwebsite\Excel\Facades\Excel::load($realPath, function($reader) {
                    $data = $reader->getSheet(0)->toArray();
                    foreach($data as $key=>$val){
                        if($key != 0){
                            $res = DB::table('assistant_copy')->where('activity_name',$val[0])->first();//查找是否有相同数据
                            if(empty($res)){
                                //对应表中字段
                                $temp[$key]['activity_name'] = $val[0];
                                $temp[$key]['activity_content'] = $val[1];
                                $temp[$key]['img_path'] = $val[2];
                                $temp[$key]['activity_time'] = $val[4];
                                $temp[$key]['activity_address'] = $val[3];
                                $temp[$key]['activity_num'] = $val[5];
                                $temp[$key]['state'] = $val[6];
                                $temp[$key]['user_id'] = $val[7];
                                $temp[$key]['activity_type'] = $val[8];
                                $temp[$key]['create_time'] = time();
                                $temp[$key]['update_time'] = time();
                            }
                        }
                    }
                    //有相同数据不导入，没有相同数据就导入
                    if(!empty($temp)){
                        DB::table('assistant_copy')->insert($temp);
                        $status = 1;
                        $message = "文件导入成功";
                    }else{
                        $status = 0;
                        $message = '文件导入失败';
                    }
                    return self::showMsg($status, $message);
                });

            }
        }
        return view('member.import');
    }
    //图片（文件）上传，jpg|png|gif|jpeg|doc|docx|xls|xlsx
    public function upload(Request $request){
        if($request->isMethod('post')) {
            //dd($_FILES['file']);
            //上传图片具体操作
            $file_name = $_FILES['file']['name'];
            //$file_type = $_FILES["file"]["type"];
            $file_tmp = $_FILES["file"]["tmp_name"];
            $file_error = $_FILES["file"]["error"];
            $file_size = $_FILES["file"]["size"];
            if ($file_error > 0) { // 出错
                $message = $file_error;
            } elseif ($file_size > 1048576) { // 文件太大了
                $message = "上传文件不能大于1MB";
            } else {
                $date = date('Ymd');
                $file_name_arr = explode('.', $file_name);
                $new_file_name = date('YmdHis') . '.' . $file_name_arr[1];
                $path = public_path('/uploads/'. $date . "/");//public文件夹下
                $file_path = $path . $new_file_name;
                if (file_exists($file_path)) {
                    $message = "此文件已经存在啦";
                } else {
                    //TODO 判断当前的目录是否存在，若不存在就新建一个!
                    if (!is_dir($path)) {
                        mkdir($path, 0777,true);
                    }
                    $upload_result = move_uploaded_file($file_tmp, $file_path);
                    //此函数只支持 HTTP POST 上传的文件
                    if ($upload_result) {
                        $status = 1;
                        $message = $_SERVER['HTTP_HOST'].'/uploads/'.$date . "/".$new_file_name;//图片路径地址：demo.blog.com/uploads/20190726/20190726080911.jpg
                    } else {
                        $message = "文件上传失败，请稍后再尝试";
                    }
                }
            }
            return self::showMsg($status, $message);
        }
        return view('member.upload');
    }
    function showMsg($status,$message = '',$data = array()){
        $result = array(
            'status' => $status,
            'message' =>$message,
            'data' =>$data
        );
        exit(json_encode($result));
    }
    public function upload1(Request $request){
        $message = '';
        if($request->isMethod('post')){
            $file_name = $_FILES['img']['name'];
            //$file_type = $_FILES["file"]["type"];
            $file_tmp = $_FILES["img"]["tmp_name"];
            $file_error = $_FILES["img"]["error"];
            $file_size = $_FILES["img"]["size"];
            if ($file_error > 0) { // 出错
                $message = $file_error;
            } elseif ($file_size > 1048576) { // 文件太大了
                $message = "上传文件不能大于1MB";
            } else {
                $date = date('Ymd');
                $file_name_arr = explode('.', $file_name);
                $new_file_name = date('YmdHis') . '.' . $file_name_arr[1];
                $path = public_path('/uploads/'. $date . "/");//public文件夹下
                $file_path = $path . $new_file_name;
                if (file_exists($file_path)) {
                    $message = "此文件已经存在啦";
                } else {
                    //TODO 判断当前的目录是否存在，若不存在就新建一个!
                    if (!is_dir($path)) {
                        mkdir($path, 0777,true);
                    }
                    $upload_result = move_uploaded_file($file_tmp, $file_path);
                    //此函数只支持 HTTP POST 上传的文件
                    if ($upload_result) {
                        //$status = 1;
                        $message = $_SERVER['HTTP_HOST'].'/uploads/'.$date . "/".$new_file_name;//图片路径地址：demo.blog.com/uploads/20190726/20190726080911.jpg
                    } else {
                        $message = "文件上传失败，请稍后再尝试";
                    }
                }
            }
            return $message;
        }
        return view('member.upload1',['message'=>$message]);
    }
    public function upload2(Request $request){
        if($request->isMethod('post')){
            $file = $request->file('picture');
            // 文件是否上传成功
            if ($file->isValid()) {

                // 获取文件相关信息
                $originalName = $file->getClientOriginalName(); // 文件原名
                $ext = $file->getClientOriginalExtension();     // 扩展名
                $realPath = $file->getRealPath();   //临时文件的绝对路径
                $type = $file->getClientMimeType();     // image/jpeg

                // 上传文件
                $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . '.' . $ext;
                // 使用我们新建的uploads本地存储空间（目录）
                //这里的uploads是配置文件的名称
                $bool = Storage::disk('uploads')->put($filename, file_get_contents($realPath));
                //var_dump($bool);

            }
            return '上传成功';
        }
        return view('member.upload2');
    }

}