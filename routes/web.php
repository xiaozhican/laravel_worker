<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Excel导出
Route::get('export','MemberController@export')->name('export');
Route::any('exportexcel','MemberController@exportexcel')->name('exportexcel');
Route::get('download','MemberController@download')->name('download');
Route::get('download1','MemberController@download1')->name('download1');
Route::any('upload','MemberController@upload')->name('upload');
Route::any('upload1','MemberController@upload1')->name('upload1');
Route::any('upload2','MemberController@upload2')->name('upload2');
Route::any('import','MemberController@import')->name('import');
//Excel导入
Route::get('/excel/import','MemberController@import')->name('/excel/import');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
