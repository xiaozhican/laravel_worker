<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/8/3
 * Time: 17:19
 */?>
<html>
<head>
    <meta charset="UTF-8">
    <title>验证码</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://upcdn.b0.upaiyun.com/libs/jquery/jquery-2.0.2.min.js"></script>
    <script src="/layui/layui.js"></script>
</head>
<body>
<div class="row">
    <div class="col-md-8">
        <input type="text" class="form-control {{$errors->has('captcha')?'parsley-error':''}}" name="captcha" placeholder="captcha">
    </div>
    <div class="col-md-4">
        <img src="{{captcha_src()}}" style="cursor: pointer" onclick="this.src='{{captcha_src()}}'+Math.random()">
    </div>
    @if($errors->has('captcha'))
        <div class="col-md-12">
            <p class="text-danger text-left"><strong>{{$errors->first('captcha')}}</strong></p>
        </div>
    @endif
</div>
</body>
</html>