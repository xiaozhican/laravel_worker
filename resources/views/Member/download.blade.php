<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/7/26
 * Time: 14:13
 */?>
<html>
<head>
    <meta charset="UTF-8">
    <title>文件导出</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://upcdn.b0.upaiyun.com/libs/jquery/jquery-2.0.2.min.js"></script>
    <script src="/layui/layui.js"></script>
</head>
<body>
<div class="layui-container">
    <form class="layui-form" action="">
        <div class="layui-row" style="text-align: center">
            <div class="layui-col-lg4">
                <div class="layui-form-item">
                    <label class="layui-form-label">活动名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="activity_name" lay-verify="required" placeholder="请输入活动名称" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-col-lg4">
                <div class="layui-form-item">
                    <label class="layui-form-label">活动地点</label>
                    <div class="layui-input-block">
                        <input type="text" name="activity_address"  lay-verify="required" placeholder="请输入活动地点" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-col-lg4">
                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block">
                        <select name="state" lay-verify="">
                            <option value="">全部</option>
                            <option value="0">进行中</option>
                            <option value="1">已结束</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="layui-col-lg4">
                <div class="layui-inline"> <!-- 注意：这一层元素并不是必须的 -->
                    时间：
                    <input type="text" class="layui-input" id="test1">
                </div>
            </div>
            <div  class="layui-col-lg4">
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <a href="{{action('MemberController@download',['export'=>1])}}" class="layui-btn layui-btn-warm">导出</a>
                        <button class="layui-btn" lay-submit lay-filter="formDemo">查询</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<table class="layui-table" lay-even="" lay-skin="row">
    <colgroup>
        <col width="20">
        <col width="50">
    </colgroup>
    <thead>
    <tr>
        <th>ID</th>
        <th>活动名称</th>
        <th>活动内容</th>
        <th>图片地址</th>
        <th>活动时间</th>
        <th>活动地点</th>
        <th>活动人数</th>
        <th>状态</th>
        <th>发布者的用户id</th>
        <th>报名参加的用户id</th>
        <th>创建时间</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->activity_name}}</td>
            <td>{{$item->activity_content}}</td>
            <td>{{$item->img_path}}</td>
            <td>{{$item->activity_time}}</td>
            <td>{{$item->activity_address}}</td>
            <td>{{$item->activity_num}}</td>
            @if($item->state==0)
                <td>进行中</td>
            @else
                <td>已结束</td>
            @endif
            <td>{{$item->user_id}}</td>
            <td>{{$item->activity_type}}</td>
            <td>{{date('Y-m-d H:i:s',$item->create_time)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $list->links('common.pagination1') }}
<script>
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#test1' //指定元素
            //,type: 'datetime'
            ,range: true //或 range: '~' 来自定义分割字符
        });
    });
</script>
</body>
</html>