<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/7/29
 * Time: 14:12
 */?>
<html>
<head>
    <meta charset="UTF-8">
    <title>文件导入</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="https://upcdn.b0.upaiyun.com/libs/jquery/jquery-2.0.2.min.js"></script>
    <script src="/layui/layui.js"></script>
</head>
<body>
<div class="layui-form-item">
    <div class="layui-upload">
        <button type="button" name="img_upload" class="layui-btn btn_upload_img">
            <input type="hidden" name="_token" class="tag_token" value="<?php echo csrf_token(); ?>">
            <i class="layui-icon"></i>文件导入</button>
        <p id="demoText"></p>
    </div>
</div>
<div>
    {{--显示图片（文件）路径--}}
    <p class="lj"></p>
</div>
<script type="text/javascript">
    layui.use('upload', function(){
        var upload = layui.upload;
        var tag_token = $(".tag_token").val();
        //普通图片上传
        var uploadInst = upload.render({
            elem: '.btn_upload_img'
            ,type : 'images|file'
            ,exts: 'jpg|png|gif|jpeg|doc|docx|xls|xlsx' //设置一些后缀，用于演示前端验证和后端的验证
            //,auto:false //选择图片后是否直接上传
            //,accept:'images' //上传文件类型
            ,url: '{{route('import')}}'
            ,data:{'_token':tag_token}
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('.img-upload-view').attr('src', result); //图片链接（base64）
                });
            }
            ,done: function(res){
                //如果上传失败
                if(res.status == 1){
                    $('.lj').html(res.message);
                    return layer.msg(res.message);
                }else{//上传成功
                    layer.msg(res.message);
                }
            }
            ,error: function(){
                //演示失败状态，并实现重传
                return layer.msg('上传失败,请重新上传');
            }
        });
    });
</script>
</body>
</html>